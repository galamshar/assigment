package com.company;

import com.company.roles.Admin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class MyApplication {
    public static FileRepository fileRepo;
    private Scanner sc = new Scanner(System.in);
    private User foundedUser;
    private Admin foundedUserAdmin;
    private User signedUser;
    private Admin signedUserAdmin;

    public FileRepository getFileRepo() {
        return fileRepo;
    }

    public void start() throws IOException {
        System.out.println("Program has been started");
        fileRepo = new FileRepository();
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }
    }

    private void menu() throws IOException {
        while (true) {
            if (signedUser == null && signedUserAdmin == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            } else {
                if (checkAdmin()) {
                    adminProfile();
                } else {
                    userProfile();
                }
            }
        }
    }

    private void logOff() throws IOException {
        signedUser = null;
        signedUserAdmin = null;
        authentication();
    }

    private void authentication() throws IOException {
        System.out.println("What you want do? : ");
        System.out.println("1. Sign In");
        System.out.println("2. Sign Up");
        System.out.println("0. Exit");
        int choice = -1;
        choice = sc.nextInt();
        if (choice == 1) {
            signIn();
        } else if (choice == 2) {
            signUp();
        } else if (choice == 0) {
            menu();
        } else {
            System.out.println("Incorrect choice,going to back...");
            menu();
        }
    }

    private void signIn() throws IOException {
        System.out.println("Please,enter your username : ");
        String username = sc.next();
        if (checkUsername(username).equals("User") || checkUsername(username).equals("Admin")) {
            System.out.println("User found! Please,enter your password : ");
            String password = sc.next();
            while (!checkPassword(password)) {
                System.out.println("Incorrect password try again : ");
                System.out.println("0.Exit");
                password = sc.next();
                if (password.equals("0")) authentication();
            }
            System.out.println("Success! Going to User Profile");
            if (checkAdmin()) {
                adminProfile();
            } else {
                userProfile();
            }
        } else if (checkUsername(username).equals("Banned")) {
            System.out.println("You are banned!");
            menu();
        } else {
            System.out.println("Such user does not exist.");
            authentication();
        }
    }

    private void signUp() throws IOException {
        System.out.println("Hello guest,register now!" + "\n" + "For canceling register enter - 0");
        System.out.println("Please,enter your username : ");
        String username = sc.next();
        while (checkUsername(username).equals("User") || checkUsername(username).equals("Admin")) {
            System.out.println("This nickname already used,enter another : ");
            username = sc.next();
        }
        if (username.equals("0")) {
            return;
        }
        System.out.println("Please,enter your password : ");
        String passwordStr = sc.next();
        while (!Password.isPwd(passwordStr)) {
            System.out.println("Use another password");
            passwordStr = sc.next();
        }
        if (passwordStr.equals("0")) {
            return;
        }
        System.out.println("Please,enter your name : ");
        String name = sc.next();
        if (name.equals("0")) {
            return;
        }
        System.out.println("Please,enter your surname : ");
        String surname = sc.next();
        if (surname.equals("0")) {
            return;
        }
        Password password = new Password(passwordStr);
        User user = new User(name, surname, username, password, "User");
        addUser(user);
        signedUser = user;
        System.out.println("Hello" + " " + signedUser.getUsername() + "!");
        userProfile();

    }

    private String checkUsername(String username) {
        for (User user : fileRepo.users) {
            if (user.getUsername().equals(username)) {
                if (user.getRole().equals("Banned")) {
                    return "Banned";
                }
                foundedUser = user;
                return "User";
            }
        }
        for (Admin admin : fileRepo.admins) {
            if (admin.getUsername().equals(username)) {
                foundedUserAdmin = admin;
                return "Admin";
            }
        }
        return "None";
    }

    private boolean checkPassword(String password) {
        if (checkAdmin()) {
            if (foundedUserAdmin.getPassword().getPasswordStr().equals(password)) {
                signedUserAdmin = foundedUserAdmin;
                return true;
            }
        } else {
            if (foundedUser.getPassword().getPasswordStr().equals(password)) {
                signedUser = foundedUser;
                return true;
            }
        }
        return false;
    }

    private boolean checkAdmin() {
        if (foundedUserAdmin != null) {
            return true;
        } else {
            return false;
        }
    }

    private void addUser(User user) throws IOException {

        fileRepo.users.add(user);
        saveUserList();
    }

    private void userProfile() throws IOException {
        System.out.println("\n");
        System.out.println("Hello" + " " + signedUser.getUsername() + "!");
        System.out.println("Availaibe actions : ");
        System.out.println("1.Get Full Information" + "\n" + "2.Change Password" + "\n" + "0.Logout");
        int choice = sc.nextInt();
        if (choice == 1) {
            System.out.println(signedUser);
        } else if (choice == 2) {
            changePassword();
        } else if (choice == 0) {
            System.out.println("Logging out");
            logOff();
        }
    }

    private void adminProfile() throws IOException {
        System.out.println("\n");
        System.out.println("Hello" + " " + signedUserAdmin.getUsername() + "!");
        System.out.println("Availaibe actions : ");
        System.out.println("1.Get Full Information" + "\n" + "2.Change Password" + "\n" + "3.User Manipulate" + "\n" + "0.Logout");
        int choice = sc.nextInt();
        if (choice == 1) {
            System.out.println(signedUserAdmin);
        } else if (choice == 2) {
            changePassword();
        } else if (choice == 3) {
            userManipulate();
        } else if (choice == 0) {
            System.out.println("Logging out");
            logOff();
        }
    }

    private void userManipulate() throws IOException {
        System.out.println("Select user which you want edit : ");
        for (User user : signedUserAdmin.getUserList()) {
            System.out.println(user.getId() + " " + user.getUsername());
        }
        System.out.println("0.Back");
        int choice = sc.nextInt();
        if (choice != 0) {
            userManipulateMenu(signedUserAdmin.getUserByID(choice));
        } else {
            adminProfile();
        }

    }

    private void userManipulateMenu(User user) throws IOException {
        System.out.println("Select option for user: ");
        System.out.println("1.Get Full Information" + "\n" + "2.Change Password" + "\n" + "3.Ban / Unban user" + "\n" + "4.Delete user" + "\n" + "0.Back");
        int choice = sc.nextInt();
        if (choice == 1) {
            System.out.println(user);
            userManipulateMenu(user);
        } else if (choice == 2) {
            changePassword(user);
            userManipulateMenu(user);
        } else if (choice == 3) {
            if (signedUserAdmin.banOrUnBanUser(user)) {
                saveUserList();
                System.out.println("Successfully banned or unbanned");
            } else {
                System.out.println("Something going wrong");
            }
            userManipulate();
        } else if (choice == 4) {
            if (signedUserAdmin.deleteUser(user)) {
                saveUserList();
                System.out.println("Successfully deleted");
            } else {
                System.out.println("Something going wrong");
            }
            userManipulate();
        } else {
            userManipulate();
        }

    }

    private void changePassword() throws IOException {
        System.out.println("0. Back");
        System.out.println("Write new password : ");
        String newPasswordStr = sc.next();
        if (!checkAdmin()) {
            if (!newPasswordStr.equals("0")) {
                if (!signedUser.getPassword().getPasswordStr().equals(newPasswordStr)) {
                    if (Password.isPwd(newPasswordStr)) {
                        Password newPassword = new Password(newPasswordStr);
                        signedUser.setPassword(newPassword);
                        System.out.println("Password successfully changed.");
                        saveUserList();
                    } else {
                        changePassword();
                    }
                } else {
                    System.out.println("New password equal to current password!");
                    changePassword();
                }
            } else {
                userProfile();
            }
        } else {
            if (!newPasswordStr.equals("0")) {
                if (!signedUserAdmin.getPassword().getPasswordStr().equals(newPasswordStr)) {
                    if (Password.isPwd(newPasswordStr)) {
                        Password newPassword = new Password(newPasswordStr);
                        signedUserAdmin.setPassword(newPassword);
                        System.out.println("Password successfully changed.");
                        saveUserList();
                    } else {
                        changePassword();
                    }
                } else {
                    System.out.println("New password equal to current password!");
                    changePassword();
                }
            } else {
                adminProfile();
            }
        }
    }

    private void changePassword(User user) throws IOException {
        System.out.println("0. Back");
        System.out.println("Write new password : ");
        String newPasswordStr = sc.next();
        if (!newPasswordStr.equals("0")) {
            if (!user.getPassword().getPasswordStr().equals(newPasswordStr)) {
                if (Password.isPwd(newPasswordStr)) {
                    Password newPassword = new Password(newPasswordStr);
                    user.setPassword(newPassword);
                    System.out.println("Password successfully changed.");
                    saveUserList();
                } else {
                    changePassword(user);
                }
            } else {
                System.out.println("New password equal to current password!");
                changePassword(user);
            }
        } else {
            userManipulateMenu(user);
        }
    }

    private void saveUserList() throws IOException {
        String content = "";
        fileRepo.users.sort(User::compareTo);
        for (User user : fileRepo.users) {
            content += user.getId() + " " + user.getName() + " " + user.getSurname() + " "
                    + user.getUsername() + " " + user.getPassword() + " " + user.getRole() + "\n";
        }
        for (Admin admin : fileRepo.admins) {
            content += admin.getId() + " " + admin.getName() + " " + admin.getSurname() + " "
                    + admin.getUsername() + " " + admin.getPassword() + " " + admin.getRole() + "\n";
        }
        Files.write(Paths.get("db.txt"), content.getBytes());
    }
}
