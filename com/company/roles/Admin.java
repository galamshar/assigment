package com.company.roles;

import com.company.Password;
import com.company.User;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import static com.company.MyApplication.fileRepo;

public class Admin extends User {
    public Admin(int id, String name, String surname, String username, Password password, String role) throws FileNotFoundException {
        super(id, name, surname, username, password, role);
    }

    public ArrayList<User> getUserList() {
        return fileRepo.getUsers();
    }

    public User getUserByID(int id) {
        for (User user : fileRepo.getUsers()) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public boolean banOrUnBanUser(User user) {
        if (user.getRole().equals("User")) {
            return fileRepo.banUser(user);
        } else {
            return fileRepo.unBanUser(user);
        }
    }

    public boolean deleteUser(User user) {
        return fileRepo.deleteUser(user);
    }
}
