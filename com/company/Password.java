package com.company;

public class Password {

    private String passwordStr;
    private static final String UPPER_CASE_CHARS = "(.*[A-Z].*)";
    private static final String LOWER_CASE_CHARS = "(.*[a-z].*)";
    private static final String NUMBERS = "(.*[0-9].*)";
    private static final String SPECIAL_CHARS = "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)";

    public Password(String passwordStr) {
        setPasswordStr(passwordStr);
    }

    public String getPasswordStr() {
        return passwordStr;
    }

    public void setPasswordStr(String passwordStr) {
        if (isPwd(passwordStr)) {
            this.passwordStr = passwordStr;
        }
    }

    public static boolean isPwd(String passwordStr) {
        if (passwordStr.length() >= 9) {
            if (!hasUpperCase(passwordStr)) {
                System.out.println("Password must have a uppercase letter");
                return false;
            } else if (!hasLowerCase(passwordStr)) {
                System.out.println("Password must have a lowercase letter");
                return false;
            } else if (!hasNumber(passwordStr)) {
                System.out.println("Password must have a digit");
                return false;
            } else if (!hasSpecialChars(passwordStr)) {
                System.out.println("Password must have a special char");
                return false;
            } else {
                return true;
            }
        } else {
            System.out.println("Length of password must be more than 9 symbols");
            return false;
        }
    }

    private static boolean hasUpperCase(String passwordStr) {
        return passwordStr.matches(UPPER_CASE_CHARS);
    }

    private static boolean hasLowerCase(String passwordStr) {

        return passwordStr.matches(LOWER_CASE_CHARS);
    }


    private static boolean hasNumber(String passwordStr) {

        return passwordStr.matches(NUMBERS);
    }

    private static boolean hasSpecialChars(String passwordStr) {

        return passwordStr.matches(SPECIAL_CHARS);
    }

    @Override
    public String toString() {
        return getPasswordStr();
    }
}
