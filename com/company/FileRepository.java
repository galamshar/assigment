package com.company;

import com.company.roles.Admin;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileRepository {
    ArrayList<User> users = new ArrayList<>();
    ArrayList<Admin> admins = new ArrayList<>();


    public FileRepository() throws FileNotFoundException {
        File file = new File("db.txt");
        Scanner fileScanner = new Scanner(file);
        while (fileScanner.hasNext()) {
            int id = fileScanner.nextInt();
            String name = fileScanner.next();
            String surname = fileScanner.next();
            String username = fileScanner.next();
            Password password = new Password(fileScanner.next());
            String role = fileScanner.next();
            if (role.equals("User")) {
                User user = new User(id, name, surname, username, password, role);
                users.add(user);
            } else if (role.equals("Admin")) {
                Admin admin = new Admin(id, name, surname, username, password, role);
                admins.add(admin);
            }


        }
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public boolean banUser(User user) {
        try {
            user.setRole("Banned");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean unBanUser(User user) {
        try {
            user.setRole("User");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean deleteUser(User user){
        return users.remove(user);
    }
}
